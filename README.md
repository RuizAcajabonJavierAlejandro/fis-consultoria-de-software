# LA CONSULTORIA
```plantuml
@startmindmap
* La consultoria
**_ Es
*** Un servicio profesional que proporciona propuestas y sugerencias \n concretas a los empresarios para resolver los problemas prácticos \n que tienen en sus organizaciones, depositando su total confianza \n en los profesionistas internos o externos a la empresa
****[#lightgreen] SERVICIOS
*****_ tales como
******[#lightblue] Social
******[#lightblue] Mobilidad
******[#lightblue] Analitica
******[#lightblue] Nube
****[#lightgreen] CARACTERISTICAS
*****_ son
******[#lightblue] Es un servicio independiente
******[#lightblue] Es un servicio consultivo
******[#lightblue] Proporciona conocimientos y capacidades profesionales para resolver problemas prácticos
******[#lightblue] No proporciona soluciones milagrosas
****[#lightgreen] OBJETIVOS
*****_ se centran en
******[#lightblue] Solucionar problemas
******[#lightblue] Identificar áreas de oportunidad en distintos departamentos de la empresa
******[#lightblue] Brindar herramientas y aprendizajes que propicien un cambio
****[#lightgreen] PRESENTE Y FUTURO
*****[#lightblue] Tasa de creación de empleo del 10,8%
*****[#lightblue] Crecimiento de un 15% en los últimos años 
*****[#lightblue] Remuneración por encima de la media
@endmindmap
```mindmap
```
# CONSULTORIA DE SOFTWARE
```plantuml
@startmindmap
*  CONSULTORIA DE SOFTWARE
**[#lightgreen] DESARROLLO
***_ se encarga
**** Jefe de proyecto
*****[#lightblue] Encargado de liderar las etapas del proyecto
*****[#lightblue] Coordina las actividades y equipo
*****[#lightblue] Se comunica con el cliente
*****[#lightblue] Establece una manera de desarrollar el proyecto
***_ tiene un
**** Proceso
*****_ que inicia con
****** Necesidad de un cliente
*******_ para despúes
******** Examinar los requisitos necesarios
********* Inicia la consultoria
**********[#lightblue] Viabilidad
**********[#lightblue] Diseño funcional
**********[#lightblue] Diseño técnico
**[#lightgreen] ACTIVIDADES
*** Necesidad de la empresa
**** Infraestructura funcional
*****_ como
******[#lightblue] Servidores
******[#lightblue] Computadoras
******[#lightblue] Comunicaciones
******[#lightblue] Seguridad
*** Funcionalidad de la empresa
****[#lightblue] Actualizar constantemente las aplicaciones para evitar fallos
****[#lightblue] Asegurar tener una infraestructura suficiente para el proyecto
****[#lightblue] Monitorear regularmente los procesos
****[#lightblue] Corroborar la inexistencia de fallos o bugs en el software
*** Factor escala
**** Tipos de clientes
*****[#lightblue] Grandes
*****[#lightblue] Pequeños
*****[#lightblue] Conocimiento total de las necesidades del cliente
**** Entorno
****[#lightblue] Ser competentes en otras áreas que no sea informática
**[#lightgreen] ACTUALIDAD Y FUTURO
***[#lightblue] Automatización acelerada de los trabajos
***[#lightblue] Se gestiona la información de la empresa de una manera más eficiente
***[#lightblue] Mayor uso de la nube
***[#lightblue] BIGDATA
***[#lightblue] Concepto de infraestructura renovado
@endmindmap
```mindmap
```
# APLICACIÓN DE LA INGENIERIA DE SOFTWARE
```plantuml
@startmindmap
* APLICACIÓN DE LA INGENIERIA DE SOFTWARE
**[#lightgreen] INTEGRA
***[#lightblue] Matemáticas
***[#lightblue] Ciencias de la computación
***[#lightblue] Prácticas
****_ en la
*****[#lightblue] Ingenieria
**_ aplicacion de
***[#lightgreen] ENFOQUE
****[#lightblue] Sistemático
****[#lightblue] Disciplinado
****[#lightblue] Cuantificable
****_ pasos
*****[#lightblue] Diseño funcional
*****[#lightblue] Conocimiento sobre los pasos a realizar
*****[#lightblue] Saber las necesidades del cliente
***[#lightgreen] DESARROLLO
****_ de
***** Software
******_ que debe ser
*******[#lightblue] Confiable
*******[#lightblue] Eficiente
*******[#lightblue] Cumplidor con el cliente y sus necesidades
**_ tiene
***[#lightgreen] 3 FASES
**** Análisis
*****[#lightblue] Conocer e interpretar los requerimientos
**** Constucción
*****[#lightblue] Desarrollo con todos los requerimientos
**** Pruebas
*****[#lightblue] Detección y corrección de errores
@endmindmap




